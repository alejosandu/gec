<?php

namespace gec\Libs;

use gec\Libs\fpdf\FPDF;

/**
 * Description of HelperPDF
 *
 * @author Zed
 */
class HelperPDF {
    
    public $FPDF;
    
    public function __construct() {
        $this->FPDF = new FPDF();
    }
    
    public function setFont($family = 'Arial', $style = 'B', $size = 12){
        $this->FPDF->SetFont($family , $style , $size);
    }
    
    public function addPage(){
        $this->FPDF->AddPage();
    }
    
    public function export(){
        return $this->FPDF->Output();
    }
    
}
