<?php
namespace gec\Core;

/**
 * Description of Controller
 *
 * @author Zed
 */
class Controller {
    
    /**
     * Función para renderizar las vistas
     * @param string $filename Nombre del archivo a buscar
     * @param boolean $renderTemplates <b>true:</b> para renderizar footer, <b>false:</b> para omitirlo
     * @param boolean $renderMenu <b>true:</b> renderiza el menu principal, <b>false:</b> para omitirlo
     */
    public function renderView($filename = "index" , $renderTemplates = true , $renderMenu = true){
        require APP . 'view/_templates/header.php';
        $renderMenu ? require APP . 'view/_templates/menu.php' : null;
//        require APP . 'view/' . $filename . '.php';
        $renderTemplates ? require APP . 'view/_templates/footer.php' : null;
        require APP . 'view/_templates/bodyEnd.php';
    }
    
    
    
}
